#include <LiquidCrystal_I2C.h>
#include <NewPing.h>
#include <HX711.h>

#define TRIGGER_PIN  3  // Arduino pin tied to trigger pin on the ultrasonic sensor.
#define ECHO_PIN     2  // Arduino pin tied to echo pin on the ultrasonic sensor.
#define MAX_DISTANCE 200 // Maximum distance we want to ping for (in centimeters). Maximum sensor distance is rated at 400-500cm.
#define DOUT 10
#define CLK 9
#define calibration_factor -363000

#define GOT_HERE()  do {Serial.print('['); Serial.print(__LINE__); Serial.println(']'); Serial.flush();} while(0)
LiquidCrystal_I2C lcd(0x27, 20, 4);
NewPing sonar(TRIGGER_PIN, ECHO_PIN, MAX_DISTANCE); // NewPing setup of pins and maximum distance.
HX711 scale;

float temp = 24; // Temperature in Celsius (this value would probably come from a temperature sensor).
float factor = sqrt(1 + temp / 273.15) / 60.368; // Speed of sound calculation based on temperature.
int distance = 0;
float nanoLoadCell = 0.0;
uint64_t previousMillis = 0;

void setup(){
  Serial1.begin(9600);    // This is the remote device
  Serial.begin(19200);  // This is the serial monitor
  Serial.println("Hello from Nano 33 BLE Sense");

  lcd.init();
  pinMode(TRIGGER_PIN, OUTPUT); // Sets the trigPin as an OUTPUT
  pinMode(ECHO_PIN, INPUT); // Sets the echoPin as an INPUT
  scale.begin(DOUT, CLK);
  scale.set_scale(calibration_factor); //This value is obtained by using the SparkFun_HX711_Calibration sketch
  scale.tare(); //Assuming there is no weight on the scale at start up, reset the scale to 
}

void loop(){
  static uint32_t last_tick;
  static const int interval = 5000;  // ... in ms
  const uint32_t current_tick = millis();

  distance = sonar.ping_median(5) * factor;
  nanoLoadCell = scale.get_units();
  // First check data from the remote device
  if (Serial1.available()){
    // Serial.write(Serial1.read());
    last_tick = current_tick;
  }else{
    // Then send data if nothing awaits
    if (current_tick - last_tick > interval){
      last_tick = current_tick;
      // Serial.println("\nSending ping request...");
      // Serial1.print("Ping...\n");
      // Serial.print("Response: ");
      if(nanoLoadCell < 0.00)
        nanoLoadCell = 0.00;
      Serial1.print(distance);
      Serial1.print(',');
      Serial1.println(nanoLoadCell);
      GOT_HERE();
      lcd.backlight();
      lcd.setCursor(0, 0);
      lcd.print("Distance:");
      lcd.setCursor(10,0);
      lcd.print(distance);
      lcd.print("cm");
      lcd.setCursor(0, 1);
      lcd.print("Level");
      lcd.setCursor(6, 1);
      if (distance > 0 && distance <= 3){
          lcd.print("XXXXXXXXXX");
      }else if(distance > 3 && distance <= 6){
          lcd.print("XXXXXXXXX");
      }else if(distance > 6 && distance <= 9){
          lcd.print("XXXXXXXX");
      }else if (distance > 9 && distance < 12){
          lcd.print("XXXXXXX");
      }else if (distance > 12 && distance < 15){
          lcd.print("XXXXXX");
      }else if (distance > 15 && distance < 18){
          lcd.print("XXXXX");
      }else if (distance > 18 && distance < 21){
          lcd.print("XXXX");
      }else if (distance > 21 && distance < 24){
          lcd.print("XXX");
      }else if (distance > 24 && distance < 27){
          lcd.print("XX");
      }else if (distance > 27 && distance < 30){
          lcd.print("X");
      }else{
          lcd.setCursor(8, 1);
          lcd.print("N/A");
      }
      lcd.clear();
    }
  }
}