#include <stdlib.h>
#include <stdio.h>
#include <Wire.h>
#include <string.h>

#include <ESP8266WiFi.h>
#include <PubSubClient.h>

#include "SparkFunCCS811.h"
#include "secrets.h"

#define pSDA 2
#define pSCL 14

#define MAX_LEN_FMSS 65
#define MAX_LEN_SEN 5

#define MLX60914_I2C_ADDR 0x5a
#define CSS811_I2C_ADDR 0x5b

#define ROW 5
#define COL 2
#define LIM ROW
#define GOT_HERE()  do {Serial.print('['); Serial.print(__LINE__); Serial.println(']'); Serial.flush();} while(0)

// Global objects for sensors, HX711, WiFi and MQTT
CCS811 myCCS811(CSS811_I2C_ADDR);
WiFiClient espClient;
PubSubClient client(espClient);

int dtVOC;
int dCO2;
int nanoHCSR04;
float nanoLoadCell;
int i = 0;

// Varaibles for MQTT messages
char mqtt_final_msg[MAX_LEN_FMSS];
const char* mqtt_server = "192.168.1.157";

// Access credentials for WiFi connection
const char* ssid = SSID;
const char* password = PASS_KEY;

void flush(){
  while(Serial.available() && Serial.read() != '\n');
}

// Connection to WiFi network
void setup_wifi() {

  delay(10);
  // We start by connecting to a WiFi network
  Serial.println();
  Serial.print("Connecting to ");
  Serial.println(ssid);

  WiFi.mode(WIFI_STA);
  WiFi.begin(ssid, password);

  while (WiFi.status() != WL_CONNECTED) {
    delay(500);
    Serial.print(".");
  }
  Serial.println("");
  Serial.println("WiFi connected");
  Serial.println("IP address: ");
  Serial.println(WiFi.localIP());
}

// MQTT function
void callback(char* topic, byte* payload, unsigned int length) {
  Serial.print("Message arrived [");
  Serial.print(topic);
  Serial.print("] ");
  for (uint8_t i = 0; i < length; i++) {
    Serial.print((char)payload[i]);
  }
  Serial.println();

  // Switch on the LED if an 1 was received as first character
  if ((char)payload[0] == '1') {
    digitalWrite(LED_BUILTIN, LOW);   // Turn the LED on (Note that LOW is the voltage level
    // but actually the LED is on; this is because
    // it is active low on the ESP-01)
  } else {
    digitalWrite(LED_BUILTIN, HIGH);  // Turn the LED off by making the voltage HIGH
  }
}

// MQTT function
void reconnect() {
  // Loop until we're reconnected
  while (!client.connected()) {
    Serial.print("Attempting MQTT connection...");
    // Create a random client ID
    // String clientId = "ESP8266Client-";
    // clientId += String(random(0xffff), HEX);
    char clientID[24] = "ESP8266Client-";
    snprintf(clientID, sizeof(clientID), "ESP8266Client-%04x", random(0xffff));
    // Attempt to connect
    if (client.connect(clientID)) {
      Serial.println("connected");
      // Once connected, publish an announcement...
      client.publish("CCS811/test", "Hello from pubsubclient");
      // ... and resubscribe
      client.subscribe("CCS811/test");
    } else {
      Serial.print("failed, rc=");
      Serial.print(client.state());
      Serial.println(" try again in 1 seconds");
      // Wait 5 seconds before retrying
      // delay(1000);
    }
  }
}

void getDatafromArduino(void){
  bool frame_ok = false;
  const int tempInt = Serial.parseInt();
  // Serial.print("Dist: ");
  // Serial.println(tempInt);
  if (Serial.read() == ','){
    // GOT_HERE();
    // First part of the frame was correct, store both numbers
    const float tempFloat = Serial.parseFloat();
    // Serial.print("Load: ");
    // Serial.println(tempFloat);
    // Check the end of the frame, only validate the frame
    // if it ends the expected way
    if (Serial.read() == '\r'){
      // GOT_HERE();
      nanoHCSR04 = tempInt;
      nanoLoadCell = tempFloat;
      frame_ok = true;
    }
  }
  if (!frame_ok){
    // Skip previous reading and flush till the next end of line
    // also warning there's been a frame out-of-sync
    flush();
    // GOT_HERE();
  }
}

void ccs811getData(void){
  // while( myCCS811.dataAvailable() == 0);
  if(myCCS811.dataAvailable()){
    myCCS811.readAlgorithmResults(); //Calling this function updates the global tVOC and CO2 variables
    dCO2 = myCCS811.getCO2();
    dtVOC = myCCS811.getTVOC();
  }
}

void sendDataMQTT(void){
  // Send MQTT message here
  snprintf(mqtt_final_msg, sizeof(mqtt_final_msg),"{\"CO2\": %d, \"tVOC\": %d, \"Dist\": %d, \"Peso\": %.3f}",
                                                                dCO2, dtVOC, nanoHCSR04, nanoLoadCell);
  client.publish("CCS811/caix1", mqtt_final_msg);
}

void setup(){
  randomSeed(micros());
  Serial.begin(9600);    // This is the remote device
  Wire.begin(pSDA, pSCL);
  setup_wifi();
  client.setServer(mqtt_server, 58999);
  client.setCallback(callback);

  pinMode(LED_BUILTIN, OUTPUT);     // Initialize the LED_BUILTIN pin as an output

  // This begins the CCS811 sensor and prints error status of .beginWithStatus()
  CCS811Core::CCS811_Status_e returnCode = myCCS811.beginWithStatus();
  Serial.print("CCS811 begin a function to print the results. ");
  Serial.println(myCCS811.statusString(returnCode));

  // This sets the mode to 10 second reads, and prints returned error status.
  returnCode = myCCS811.setDriveMode(4);
  //Pass the error code t
  Serial.print("Mode request exited with: ");
  Serial.println(myCCS811.statusString(returnCode));
}

void loop(){
  client.setKeepAlive(90);
  if (!client.connected()){
    reconnect();
  }
  client.loop();

  if (Serial.available()){
    getDatafromArduino();
    ccs811getData();
    sendDataMQTT();
  }
}